const express = require('express');
const fs = require('fs');
const app     = express();
const htmlToJson = require('html-to-json');

app.get('/scrape', async function(req, res){

    const parser = htmlToJson.createParser(['.resource-item', {
            'date': ($el) => {
                return $el.find('h6').text();
            },
            'title': ($el) => {
                return $el.find('h5 a').text()
            },
            'link': ($el) => {
                return $el.find('a').attr('href')
            },
            'subtitle': ($el) => (
                $el.find('h4').text()
            ),
            'content': ($el) => (
                $el.find('p').text()
            )
        }]);

    const urls = [
        'https://www.californiawaterfix.com/resources/news-archive/',
        'https://www.californiawaterfix.com/resources/outreach-materials/fact-sheets',
        'https://www.californiawaterfix.com/resources/outreach-materials/faqs',
        'https://www.californiawaterfix.com/resources/outreach-materials/videos',
        'https://www.californiawaterfix.com/resources/planning-process/eir-eis/',
        'https://www.californiawaterfix.com/resources/planning-process/validation/',
        'https://www.californiawaterfix.com/resources/planning-process/permits/',
        'https://www.californiawaterfix.com/resources/design-and-construction-enterprise/',
        'https://www.californiawaterfix.com/state-water-board/'
    ];

    await urls.forEach((url,key) => {
        parser.request(url).done(function (result) {
            fs.writeFile(`./jsonOut/${key}.json`, JSON.stringify(result, null, 4), function(err){
                if (err)
                    console.log(err);
                console.log('File successfully written! - Check your project directory for the output.json file');
            })
        });
    })

    res.send('check yo files')
});


app.listen('8081');
console.log('Magic happens on port 8081');
exports = module.exports = app;